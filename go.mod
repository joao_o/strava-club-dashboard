module github.com/converge/strava-club-dashboard

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.7.0
	github.com/google/uuid v1.3.0
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/rs/zerolog v1.26.1
)

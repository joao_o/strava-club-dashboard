package repositories

import (
	"database/sql"
	"fmt"
	"github.com/converge/strava-club-dashboard/internal/services"
	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog/log"
)

type sqliteDb struct {
	*sql.DB
}

func NewSqlite() *sqliteDb {
	db, err := sql.Open("sqlite3", "../../../internal/repositories/strava-club.db")
	if err != nil {
		panic(err)
	}

	// tmp
	_, err = db.Exec(`
		DROP TABLE IF EXISTS club_activities;
		CREATE TABLE club_activities (
		    athlete_name TEXT,
		    distance INTEGER,
		    created TIMESTAMP,
		    modified TIMESTAMP
	 	)
	`)
	if err != nil {
		panic(err)
	}

	return &sqliteDb{db}
}

func (db *sqliteDb) SaveAthleteActivity(rca services.RiderClubActivity) {

	stmt, err := db.Prepare(`
		INSERT INTO club_activities (
		                             athlete_name,
		                             distance,
		                             created,
		                             modified
		                             )
			 VALUES ($1, $2, date('now'), date('now'));
	`)
	if err != nil {
		log.Err(err)
	}

	res, err := stmt.Exec(
		rca.Athlete.Firstname,
		rca.Distance,
	)
	if err != nil {
		log.Err(err)
	}
	fmt.Println(res.RowsAffected())
}

package services

import (
	"encoding/json"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"time"
)

type StravaService struct {
	*kafka.Producer
	*kafka.Consumer
}

func NewStrava(kafkaProducer *kafka.Producer, kafkaConsumer *kafka.Consumer) StravaService {
	return StravaService{
		kafkaProducer,
		kafkaConsumer,
	}
}

type RiderClubActivity struct {
	ResourceState int `json:"resource_state"`
	Athlete       struct {
		ResourceState int    `json:"resource_state"`
		Firstname     string `json:"firstname"`
		Lastname      string `json:"lastname"`
	} `json:"athlete"`
	Name               string  `json:"name"`
	Distance           float64 `json:"distance"`
	MovingTime         int     `json:"moving_time"`
	ElapsedTime        int     `json:"elapsed_time"`
	TotalElevationGain float64 `json:"total_elevation_gain"`
	Type               string  `json:"type"`
}

// ProduceKafkaMessage a
// produce a message to a specific topic
func (service StravaService) ProduceKafkaMessage(rca RiderClubActivity) error {

	topic := "strava-club-activities"
	//riderId := fmt.Sprintf("%s %s", rca.Athlete.Firstname, rca.Athlete.Lastname)
	riderId := uuid.New().String()
	encodedMsg, err := json.Marshal(rca)
	if err != nil {
		log.Err(err)
	}
	//distance := fmt.Sprintf("distance: %f", encodedMsg)
	msg := kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Key:   []byte(riderId),
		Value: encodedMsg,
	}

	go func() {
		for e := range service.Producer.Events() {
			switch event := e.(type) {
			case *kafka.Message:
				if event.TopicPartition.Error != nil {
					log.Error().Msg("delivered failed...")
				} else {
					log.Info().Msgf("delivered message to %v", event.TopicPartition)
				}
			}
		}
	}()

	err = service.Producer.Produce(&msg, nil)
	if err != nil {
		log.Err(err)
		return err
	}
	service.Producer.Flush(15 * 3000)
	return nil
}

func (service StravaService) GetKafkaMessage(topic string) ([]RiderClubActivity, error) {

	err := service.Consumer.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		log.Err(err)
		return nil, err
	}

	var riderActivities []RiderClubActivity

	// process messages
	// todo: endloop with channel
	for {
		log.Info().Msg("waiting new messages...")
		msg, err := service.Consumer.ReadMessage(10 * time.Second)
		if err == nil {
			log.Info().Msgf("Message: %s, Partition: %s", msg.Value, msg.TopicPartition)
			var ra RiderClubActivity
			err = json.Unmarshal(msg.Value, &ra)
			if err != nil {
				log.Err(err)
			}
			riderActivities = append(riderActivities, ra)
		} else {
			// will time out after 10 secs
			log.Error().Msgf("consumer error: %v (%v)", err, msg)
			break
		}
	}

	return riderActivities, nil
}

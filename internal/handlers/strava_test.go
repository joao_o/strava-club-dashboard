package handlers

import (
	"net/http"
)

func (mockedHTTPClient MockedHTTPClient) Do(req *http.Request) (*http.Response, error) {
	return nil, nil
}

type MockedHTTPClient struct{}

//func TestStravaHandler_GetClubUpdate(t *testing.T) {
//	mockedHTTPClient := MockedHTTPClient{}
//	db := repositories.NewSqlite()
//	stravaService := services.StravaService{}
//	stravaHandler := StravaHandler{
//		HTTPClient: mockedHTTPClient,
//		Store:      db,
//		Service:    stravaService,
//	}
//	err := stravaHandler.GetClubUpdate()
//	if err != nil {
//		t.Error(err)
//	}
//}
